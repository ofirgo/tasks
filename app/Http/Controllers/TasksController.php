<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;





class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function changeStatus($tid, $sid)
    {
        $task = Task::findOrFail($tid);
        $from = $task->status->id;
        if(!Status::allowed($from,$sid)) return redirect('tasks');        
        $task->status_id = $sid;
        $task->save();
        return back();
    } 

    public function myTasks()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        //$tasks = $user->tasks;
        $tasks = Task::join('task_user','tasks.id','=','task_user.task_id')->where([['status_id','1'],['user_id',Auth::id()],])->orWhere([['status_id','3'],['user_id',Auth::id()],])->paginate('2');
        $users = User::all();
        $statuses = Status::all();        
        return view('tasks.index', compact('tasks','users', 'statuses'));
    }

    public function completedTasks()
    {     
        $tasks= Task::where('status_id','4')->paginate('2');
        $statuses = Status::all();   
        $users = User::all();  
        return view('tasks.index', compact('tasks','users','statuses'));
    }

// default

    public function index()
    {
        $tasks = Task::where('status_id','1')->orWhere('status_id','3')->paginate('2');
        $users = User::all();
        $statuses=Status::all();
        return view('tasks.index', compact('tasks','users','statuses')); 
    }
   
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('tasks.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $tsk=$task->create($request->all());
        $tsk->status_id=1;
        $userId = $request->user_id;
        $tsk->users()->attach($userId); 
        $tsk->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $task = Task::findOrFail($id);
         $users = User::all();
         return view('tasks.edit', compact('task','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $task = Task::findOrFail($id);
        $task->update($request->all());  
        
        $task->users()->detach();
        $userId = $request->user_id;
        $task->users()->attach($userId); 

        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('tasks');
    }
}
