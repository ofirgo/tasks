<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Task extends Model
{
    protected $fillable = [
        'task_description', 'start_date', 'estimated_end_date', 'status_id'
    ];

        public function users(){
            return $this->belongsToMany(User::class,'task_user','task_id','user_id');
        }

        public function status(){
            return $this->belongsTo('App\Status','status_id');
        }

        // public static function done(){
        //     $tasks = DB::table('tasks')->where('status_id','4')->pluck('id');
        //     return self::find($tasks)->all(); 
        // }
  
        // public static function relevant(){
        //     $tasks = DB::table('tasks')->where('status_id','1')->orWhere('status_id','3')->paginate(2)->pluck('id');
        //     return self::find($tasks)->all(); 
        // }



     


       
       

}


