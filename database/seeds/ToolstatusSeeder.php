<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ToolstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('toolstatuses')->insert([
            [
                'name' => 'Available',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'In-Use',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'In-Repair',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Out Of Order',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            ]);   
    }
}
