@extends('layouts.app')

@section('content')
        <div><a href =  "{{url('/tasks/create')}}" > Add New Task</p></a></div>
        <h1>List Of Tasks</h1>
        
        <table class = "table">
            <tr>
                <th>Id</th><th>Description</th><th>Start Date</th><th>End Date</th><th>Employees</th><th>Status</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
            </tr>
            
            @foreach($tasks as $task)
                    <tr>
                    <td>{{$task->id}}</td>
                    <td>{{$task->task_description}}</td>
                    <td>{{$task->start_date}}</td>
                    <td>{{$task->estimated_end_date}}</td>
                    <td>
                        @foreach(App\User::employee($task->id) as $user)
                            {{$user->name}} <br>
                        @endforeach
                    </td>
                    <td>
                    <div class="dropdown">
                    @if (null != App\Status::switch($task->status_id))    
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($task->status_id))
                           {{$task->status->name ?? 'bla'}}
                        @endif
                    </button>
                    @else 
                    {{$task->status->name}}
                    @endif
                                                   
                    @if (App\Status::switch($task->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::switch($task->status_id) as $status)
                         <a class="dropdown-item" href="{{route('tasks.changestatus', [$task->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                    </div>   
                    </td>
                    <td>{{$task->created_at}}</td>
                    <td>{{$task->updated_at}}</td>
                    <td>
                        <a href = "{{route('tasks.edit',$task->id)}}">Edit</a>
                    </td>
                    <td>
                        <a href = "{{route('tasks.delete',$task->id)}}">Delete</a>
                    </td>     
                </tr>
            @endforeach
        </table>
        {{ $tasks->links() }}
       
        
@endsection
