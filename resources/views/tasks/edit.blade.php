@extends('layouts.app')

@section('title', 'Edit task')

@section('content')
        <h1>Edit Task</h1>
        <form method = "post" action = "{{action('TasksController@update', $task->id)}}">
        @csrf
        @METHOD('PATCH')
             <div class="form-group">
            <label for = "task_description">Task Description</label>
            <input type = "text" class="form-control" name = "task_description" value = {{$task->task_description}}>
        </div>     
        <div class="form-group">
            <label for = "start_date">Start Date</label>
            <input type = "date" class="form-control" name = "start_date" value = {{$task->start_date}}>
        </div>
        <div class="form-group">
            <label for = "estimated_end_date">Estimated End Date</label>
            <input type = "date" class="form-control" name = "estimated_end_date" value = {{$task->estimated_end_date}}>
        </div>
        <div class="form-group">
            <label for = "employee">Choose Employee</label>
            @foreach($users as $user)
                @if(in_array($user, App\User::employee($task->id)))
                    <input type="checkbox" name="user_id[]" value={{$user->id}} checked="true">
                    {{$user->name}} 
                @else
                    <input type="checkbox" name="user_id[]" value={{$user->id}}>
                    {{$user->name}}
                @endif
            @endforeach
        </div> 
        <div> 
        <div>
            <input type = "submit" name = "submit" value = "Update Task">
        </div>                  
        </form>  
@endsection





