@extends('layouts.app')

@section('content')
        <div><a href =  "{{url('/tools/create')}}" > Add New Tool</p></a></div>
        <h1>List Of Tools</h1>
        
        <table class = "table">
            <tr>
                <th>Id</th><th>Name</th><th>User</th><th>Status</th><th>Description</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
            </tr>
            
            @foreach($tools as $tool)
                    <tr>
                    <td>{{$tool->id}}</td>
                    <td>{{$tool->name}}</td>        
                    <td>          
                    <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($tool->user_id))
                          {{$tool->user->name}}  
                        @else
                          Assign User
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                      <a class="dropdown-item" href="{{route('tools.changeuser',[$tool->id,$user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                  </div>  
                    </td>
                    <td>{{$tool->toolstatus->name}}</td>
                    <td>{{$tool->tool_description}}</td>
                    <td>{{$tool->created_at}}</td>
                    <td>{{$tool->updated_at}}</td>
                    <td>
                        <a href = "{{route('tools.edit',$tool->id)}}">Edit</a>
                    </td>
                    <td>
                        <a href = "{{route('tools.delete',$tool->id)}}">Delete</a>
                    </td>     
                </tr>
            @endforeach
        </table>
        {{$tools->links()}}
@endsection
