@extends('layouts.app')

@section('title', 'Edit Tool')

@section('content')
        <h1>Edit Tool</h1>
        <form method = "post" action = "{{action('ToolsController@update', $tool->id)}}">
        @csrf
        @METHOD('PATCH')
             <div class="form-group">
            <label for = "name">Name</label>
            <input type = "text" class="form-control" name = "name" value = {{$tool->name}}>
        </div>     
        <div class="form-group">
            <label for = "start_date">Tool Description</label>
            <input type = "text" class="form-control" name = "tool_description" value = {{$tool->tool_description}}>
        </div>
        <div class="form-group">
            <label for = "status">Choose Status</label>


            @foreach($statuses as $status)
          
            <input  type="checkbox" name="status_id" value={{$status->id}}>
            {{$status->name}}

            @endforeach

        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Update Tool">
        </div>

        </form>  
@endsection





