@extends('layouts.app')

@section('title', 'Add tool')

@section('content')
        <h1>Create Tool</h1>
        <form method = "post" action = "{{action('ToolsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "tool_description">Tool Description</label>
            <input type = "text" class="form-control" name = "tool_description">
        </div>

        <div>
            <input type = "submit" name = "submit" value = "Add Tool">
        </div>                       
        </form>  
@endsection