@extends('layouts.app')

@section('content')
        <h1>Details</h1>
        
        <table class = "table">
            <tr>
                <th>Id</th><th>Name</th><th>Description</th><th>Created</th><th>Updated</th>
            </tr>
            
           
                    <tr>
                    <td>{{$tool->id}}</td>
                    <td>{{$tool->name}}</td>
                    <td>{{$tool->tool_description}}</td>
                    <td>{{$tool->created_at}}</td>
                    <td>{{$tool->updated_at}}</td>
                </tr>

        </table>
@endsection
