<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Tasks Routes
Route::resource('tasks', 'TasksController')->middleware('auth');
Route::get('tasks/changestatus/{id}/{sid}', 'TasksController@changeStatus')->name('tasks.changestatus');

Route::get('tasks/delete/{id}', 'TasksController@destroy')->name('tasks.delete');

Route::get('tasks/changeuser/{tid}/{uid?}', 'TasksController@changeUser')->name('task.changeuser');

Route::get('mytasks', 'TasksController@myTasks')->name('tasks.mytasks')->middleware('auth');

Route::get('completedtasks', 'TasksController@completedTasks')->name('tasks.completedtasks')->middleware('auth');


# Tools Routes
Route::resource('tools', 'ToolsController')->middleware('auth');

Route::get('tools/changeuser/{tl_id}/{uid?}', 'ToolsController@changeUser')->name('tools.changeuser');

Route::get('tools/delete/{id}', 'ToolsController@destroy')->name('tools.delete');




# Laravel Routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
